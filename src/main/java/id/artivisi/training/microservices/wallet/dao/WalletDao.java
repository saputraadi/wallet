package id.artivisi.training.microservices.wallet.dao;

import id.artivisi.training.microservices.wallet.entity.Wallet;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WalletDao extends PagingAndSortingRepository<Wallet, String> {
}
